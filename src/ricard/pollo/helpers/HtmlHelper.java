package ricard.pollo.helpers;

public class HtmlHelper {

	public static String navegacion() {
		return "	<nav class=\"navbar navbar-expand-lg navbar-light bg-light\">\r\n" + 
				"		<a class=\"navbar-brand\" href=\"#\">Pollo Web</a>\r\n" + 
				"		<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\"\r\n" + 
				"			data-target=\"#navbarNav\" aria-controls=\"navbarNav\"\r\n" + 
				"			aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n" + 
				"			<span class=\"navbar-toggler-icon\"></span>\r\n" + 
				"		</button>\r\n" + 
				"		<div class=\"collapse navbar-collapse\" id=\"navbarNav\">\r\n" + 
				"			<ul class=\"navbar-nav\">\r\n" + 
				"				<li class=\"nav-item active\"><a class=\"nav-link\" href=\"/Pollo/index.jsp\">Inicio\r\n" + 
				"						<span class=\"sr-only\">(current)</span>\r\n" + 
				"				</a></li>\r\n" + 
				"				<li class=\"nav-item active\"><a class=\"nav-link\" href=\"/Pollo/gallinas.jsp\">Gallinas</a>\r\n" + 
				"				</li>\r\n" + 
				"			</ul>\r\n" + 
				"		</div>\r\n" + 
				"	</nav>";
	}
	
}
