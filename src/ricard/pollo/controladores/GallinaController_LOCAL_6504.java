package ricard.pollo.controladores;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import ricard.pollo.modelos.*;

// gallina controller
// linia modificada en branca
public class GallinaController {

	public static List<Gallina> getGallinas(){
	
		List<Gallina> theList = new ArrayList<Gallina>();
		String sql ="select * from gallinas";
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				theList.add(new Gallina(
						rs.getInt(1),
						rs.getString(2), 
						rs.getString(3)
						));
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return theList;
	}
	
	public static String getFoto(int id){
		
		String respuesta = "nofoto.jpg";
		
		String sql =String.format("select foto from gallinas where id = %d", id);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			
			ResultSet rs = stmt.executeQuery(sql);

			if (rs.next()) {
				respuesta = rs.getString(1);
			}
		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		return respuesta;
	}
	
	
	
public static void CreaGallina(String nombre){
		
	
		String foto = "sinfoto.jpg";
		String sql =String.format("insert into gallinas (nombre, foto) values (\"%s\", \"%s\")", nombre, foto);
		
		try (Connection conn = DBConn.getConn();
				Statement stmt = conn.createStatement()) {
			
			stmt.executeUpdate(sql);

		} catch (Exception e) {
			String s = e.getMessage();
			System.out.println(s);
		}
		
	}


}
