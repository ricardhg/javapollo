package ricard.pollo.modelos;

public class Gallina {

	public int id;
	public String nombre;
	public String foto;
	
	public Gallina(int id, String nombre, String foto) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.foto = foto;
	}
	
	public Gallina(String nombre, String foto) {
		super();
		this.id = 0;
		this.nombre = nombre;
		this.foto = foto;
	}
	
}
